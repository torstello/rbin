#!/bin/env ruby

require 'clamp'

Clamp do

  # checks, if a repository has uncommited changes
  def repo_log(dir)
    Dir.chdir(dir)
    system('git log -2 HEAD --pretty=format:%s')
    puts
  end

  # print the last 2 commit messages of all modules in an environment
  def env_log(env_dir)
    modules = []
    env_dir = "/etc/puppet/environments/#{env}/modules"

    Dir.foreach(env_dir) do |d|
      modules << "#{env_dir}/#{d}"
    end

    modules.sort!

    modules.each do |mod|
      Dir.chdir(mod)
      puts mod.split('/').last.upcase
      repo_log(mod)
    end
  end

  # prints the last 2 commit messages of one module in
  def mod_log(m, environments)
    env_array.map! { |e| "/etc/puppet/environments/#{e}/modules/#{m}" }

    env_array.each do |m|
      Dir.chdir(m)
      puts "#{m.split('/').last.upcase} in environment #{env.upcase}"
      # puts "chdir into #{m}"
      repo_log(m)
    end
  end

  option '-m', '--module', 'name of the puppet module', 
           :attribute_name => 'mod_name', :default => ""

  parameter '[environment]', 'name of the puppet environment', 
              :attribute_name => 'env', :default => "production"

  def execute
    env_log(env)
  end
end

#!/bin/env ruby

require 'clamp'

Clamp do

  @home = ENV['HOME']

  # checks, if a repository has uncommited changes
  def repo_has_changes?(dir)
    Dir.chdir(dir)
    status = %x(/usr/bin/git status --porcelain 2>/dev/null)
    if status == ''
      return false
    else
      return true
    end
  end

  # prints modules with their uncommited changes based on an environment
  def env_status(repos_dir)
    puppet_modules          = []
    ansible_modules         = []
    changed_puppet_modules  = []
    changed_ansible_modules = []
    puppet_repos_dir        = "/etc/puppet/environments/#{env}/modules"
    ansible_repos_dir       = "/etc/ansible/environments/#{env}/roles"

    Dir.foreach(puppet_repos_dir) do |d|
      puppet_modules << "#{puppet_repos_dir}/#{d}"
    end
    Dir.foreach(ansible_repos_dir) do |d|
      ansible_modules << "#{ansible_repos_dir}/#{d}"
    end

    # add hiera module to puppet:
    puppet_modules << "#{puppet_repos_dir}/../hieradata"
    # add playbooks to ansible:
    ansible_modules << "#{ansible_repos_dir}/../playbooks"

    # collect repos that have uncommit'ed changes
    puppet_modules.each do |mod|
      changed_puppet_modules << mod if repo_has_changes?(mod)
    end
    # collect repos that have uncommit'ed changes
    ansible_modules.each do |mod|
      changed_ansible_modules << mod if repo_has_changes?(mod)
    end

    changed_puppet_modules.each do |mod|
      Dir.chdir(mod)
      puts('Puppet-' + mod.split('/').last.upcase)
      system('/usr/bin/git status --short')
    end
    changed_ansible_modules.each do |mod|
      Dir.chdir(mod)
      puts('Ansible-' + mod.split('/').last.upcase)
      system('/usr/bin/git status --short')
    end
  end

  parameter '[environment]', 'name of the puppet environment', :attribute_name => 'env', :default => "ts"

  def execute
    env_status(env)
  end
end
